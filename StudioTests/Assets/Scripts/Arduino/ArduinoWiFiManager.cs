﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;



public class ArduinoWiFiManager : MonoBehaviour
{


    private TCP myTCP;

    public bool sim = false;

    public string IP = "192.168.4.1";           //ip/address of the server, 127.0.0.1 is for your own computer
    public int port = 5151;                     //port for the server, make sure to unblock this in your router firewall if you want to allow external connections

    // For inspector
    public static string msgFromServer;                
    public float[] printNumbers;                
    public string errorString;
    
    public static float[] sensorValues = new float[2];
    


    void Awake()
    {
        myTCP = new TCP(IP, port);
    }



    void Start()
    {
        if (!sim)
        {
            errorString = myTCP.setupSocket();
            InvokeRepeating("SendToServer", 5, 0.2f);
        }
        else
        {
            for (int i = 0; i < sensorValues.Length; i++)
            {
                sensorValues[i] = 100;
            }
        }
    }


    void Update()
    {
        if (!sim)
        {
            myTCP.maintainConnection();
            if (myTCP.canRead)
            {
                SocketResponse();
            }
        }
    }
    

    //socket reading script
    void SocketResponse()
    {
        /////////////////////////////////////////////////
        // This is the string from the arduino, it is a number representing the cm of the rangeFinder
        string serverSays = myTCP.readSocket();

        if (serverSays != "")
        {
            msgFromServer = serverSays;
            char spliter = ',';
            string[] sensorValues = serverSays.Split(spliter);
            float[] incommingNumber = new float[sensorValues.Length];
            printNumbers = new float[sensorValues.Length];
            //Debug.Log("[SERVER]" + serverSays);
            for (int i = 0; i < sensorValues.Length; i++)
            {
                if (float.TryParse(sensorValues[i], out incommingNumber[i]))
                {
                    ArduinoWiFiManager.sensorValues[i] = incommingNumber[i];
                    printNumbers[i] = incommingNumber[i];
                    ////////////////////////////////////////////////////////
                    // Here is where the string has be converted into an int, atm it changes the Y scale of the object this script is attached to.
                    // Add what you want to do here...
                    //transform.localScale = new Vector3(transform.localScale.x, incommingNumber / 10f, transform.localScale.z);
                }
            }
        }



    }



    //send message to the server
    public void SendToServer()
    {

        if (myTCP != null && myTCP.socketReady)
        {
            myTCP.writeSocket("P");
        }

    }
    

}

