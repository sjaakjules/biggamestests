﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;



public class ArduinoBluetoothManager : MonoBehaviour
{

    public bool sim = false;
    
    // For inspector
    public string msgFromServer;                
    public float[] printNumbers;                
    public string errorString;
    
    public static float[] sensorValues = new float[2];

    bool StartConnection = true;

    /*
    void OnGUI()
    {
        if (GUILayout.Button("Connect"))
        {
            StartConnection = true;
        }
        if (StartConnection)
        {
            if (!BtConnection.isBluetoothEnabled())
            {
                BtConnection.enableBluetooth();
            }
            else
            {
                BtConnection.connect();
                StartConnection = false;
            }
        }
    }
    */

    void Awake()
    {
        if (!BtConnector.isBluetoothEnabled())
        {
            BtConnector.enableBluetooth();
        }
    }



    void Start()
    {
        if (!sim)
        {
            if (StartConnection)
            {
                //BtConnector.moduleMAC("20:16:03:25:09:85");
                BtConnector.moduleName("HC-05");
                BtConnector.connect();
                StartConnection = false;
            }
            InvokeRepeating("SendToServer", 2, 0.2f);
        }
        else
        {
            for (int i = 0; i < sensorValues.Length; i++)
            {
                sensorValues[i] = -1;
            }
        }
    }


    void Update()
    {
        if (!sim)
        {
            if (!BtConnector.isConnected()) //the Connect button will disappear when connecttion done
                                                                            // and appear again if it disconnected
            {
                if (!BtConnector.isBluetoothEnabled())
                {
                    BtConnector.enableBluetooth();
                    Debug.Log("No bluetooth");
                }
                else
                {
                    BtConnector.moduleName("HC-05");
                    BtConnector.connect();
                } 
            }

            if (BtConnector.isConnected() && BtConnector.available())
            {
                SocketResponse();
            }
        }
    }
    

    //socket reading script
    void SocketResponse()
    {
        /////////////////////////////////////////////////
        // This is the string from the arduino, it is a number representing the cm of the rangeFinder
        string serverSays = BtConnector.readLine();

        if (serverSays != "")
        {
            msgFromServer = serverSays;
            char spliter = ',';
            string[] sensorValues = serverSays.Split(spliter);
            float[] incommingNumber = new float[sensorValues.Length];
            printNumbers = new float[sensorValues.Length];
            //Debug.Log("[SERVER]" + serverSays);
            for (int i = 0; i < sensorValues.Length; i++)
            {
                if (float.TryParse(sensorValues[i], out incommingNumber[i]))
                {
                    ArduinoBluetoothManager.sensorValues[i] = incommingNumber[i];
                    printNumbers[i] = incommingNumber[i];
                    ////////////////////////////////////////////////////////
                    // Here is where the string has be converted into an int, atm it changes the Y scale of the object this script is attached to.
                    // Add what you want to do here...
                    //transform.localScale = new Vector3(transform.localScale.x, incommingNumber / 10f, transform.localScale.z);
                }
            }
        }



    }



    //send message to the server
    public void SendToServer()
    {

        if (BtConnector.isConnected())
        {
            BtConnector.sendString("P");
        }

    }
    

}

