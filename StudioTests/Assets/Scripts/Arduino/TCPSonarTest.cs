﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;



public class TCPSonarTest : MonoBehaviour
{



    //variables

    private TCPConnection myTCP;
    private string serverMsg;
    public string msgToServer;
    bool connectionEstablished = false;
    public static float[] sonarValue = new float[2];

    // Debug to inspec
    public float[] printNumbers;

    int attempts = 1;

    public bool sim = false;

    string errorString;

    void Awake()
    {

        //add a copy of TCPConnection to this game object

        myTCP = gameObject.AddComponent<TCPConnection>();

    }



    void Start()
    {
        if (!sim)
        {
            StartCoroutine(attemptConnection((string s) => setString(s, out errorString), 5f));
            InvokeRepeating("SendToServer", 5, 0.5f);
        }
        else
        {
            for (int i = 0; i < sonarValue.Length; i++)
            {
                sonarValue[i] = -1;
            }
        }
    }

    void setString(string s, out string copy)
    {
        copy = s.ToString();
    }

    void Update()
    {



        //keep checking the server for messages, if a message is received from server, it gets logged in the Debug console (see function below)
        if (connectionEstablished)
        {
            // This is called to check the communication to the arduino, and then does something with the new value.
            SocketResponse();
        }
        else
            Debug.LogError("Not Connected: \n" + errorString);

    }


    

    IEnumerator attemptConnection(Action<string> callback, float timeout = float.PositiveInfinity)
    {
        DateTime initialTime = DateTime.Now;
        DateTime nowTime;
        TimeSpan diff = default(TimeSpan);
        string dataString = "";

        do
        {
            try
            {
                dataString = dataString + "\nAttempting to connect.. " + attempts;

                myTCP.setupSocket();
                connectionEstablished = true;
                dataString = dataString + "\nConnection Established!";
            }
            catch (Exception e)
            {
                dataString = dataString + "\nError" + e.Message;
                attempts++;
            }

            if (connectionEstablished)
            {
                callback(dataString);
                yield return null;
            }
            else
            {
                callback(dataString);
                yield return new WaitForSeconds(0.05f);
            }


            nowTime = DateTime.Now;
            diff = nowTime - initialTime;

        } while (diff.Milliseconds < timeout);

        yield return null;


    }


    //socket reading script

    void SocketResponse()
    {
        if (myTCP != null)
        {

            /////////////////////////////////////////////////
            // This is the string from the arduino, it is a number representing the cm of the rangeFinder
            string serverSays = myTCP.readSocket();

            if (serverSays != "")
            {
                msgToServer = serverSays;
                char spliter = ',';
                string[] sensorValues = serverSays.Split(spliter);
                float[] incommingNumber = new float[sensorValues.Length];
                printNumbers = new float[sensorValues.Length];
                //Debug.Log("[SERVER]" + serverSays);
                for (int i = 0; i < sensorValues.Length; i++)
                {
                    if (float.TryParse(sensorValues[i], out incommingNumber[i]))
                    {
                        sonarValue[i] = incommingNumber[i];
                        printNumbers[i] = incommingNumber[i];
                        ////////////////////////////////////////////////////////
                        // Here is where the string has be converted into an int, atm it changes the Y scale of the object this script is attached to.
                        // Add what you want to do here...
                        //transform.localScale = new Vector3(transform.localScale.x, incommingNumber / 10f, transform.localScale.z);
                    }
                }
            }

        }

    }



    //send message to the server

    public void SendToServer()
    {

        if (myTCP != null)
        {

            myTCP.writeSocket("P\n");
        }
        //Debug.Log("[CLIENT] -> " + str);

    }

}

