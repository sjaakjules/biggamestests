﻿using UnityEngine;
using System.Collections;

public class RangeFinderMotion : MonoBehaviour
{
    ArduinoConnector arduino = new ArduinoConnector();
    

    UDPSend sender;
    UDPReceive receiver;

    string incomingMsg = null;
    int incomingNumber;
    bool startedCoroutine = false;

    // Use this for initialization
    void Start()
    {
        arduino.port = "COM3";
        arduino.Open();
        sender = new UDPSend("192.168.0.100", 5059);
        receiver = new UDPReceive(5058);
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            arduino.WriteToArduino("PING");
            sender.sendString("PING");
        }
        catch (System.Exception)
        {
            Debug.LogError("Error trying to write to arduino");
        }

        StartCoroutine(arduino.AsynchronousReadFromArduino((string s) => setValue(s, out incomingMsg), () => Debug.LogError("Arduino Error!"), 1f));
        startedCoroutine = true;

        if (int.TryParse(receiver.lastReceivedUDPPacket, out incomingNumber))
        {
            if (incomingNumber > 0 && incomingNumber < 400)
            {

                transform.localScale = new Vector3(transform.localScale.x, (float)1.0f * incomingNumber/1f, transform.localScale.z); ;

                //transform.position = new Vector3(transform.position.x, (float)1.0f * incomingNumber / 10, transform.position.z);
                incomingMsg = null;
                startedCoroutine = false;
            }
        }



    }

    void OnApplicationQuit()
    {
        receiver.closeThread();
    }

    void setValue(string s, out string s2)
    {
        s2 = s;
    }
}