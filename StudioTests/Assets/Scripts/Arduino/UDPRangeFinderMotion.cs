﻿using UnityEngine;
using System.Collections;

public class UDPRangeFinderMotion : MonoBehaviour
{

    UDPSend sender;
    UDPReceive receiver;
    
    int incomingNumber;

    // Use this for initialization
    void Start()
    {
        sender = new UDPSend("192.168.4.1", 5052);
        receiver = new UDPReceive(5051);
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            sender.sendString("PING");
        }
        catch (System.Exception)
        {
            Debug.LogError("Error trying to write to arduino");
        }
        

        if (int.TryParse(receiver.lastReceivedUDPPacket, out incomingNumber))
        {
            if (incomingNumber > 0 && incomingNumber < 400)
            {

                transform.localScale = new Vector3(transform.localScale.x, (float)1.0f * incomingNumber / 1f, transform.localScale.z); ;
                
            }
        }


    }

    void OnApplicationQuit()
    {
        receiver.closeThread();
    }

    void setValue(string s, out string s2)
    {
        s2 = s;
    }
}