﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;

public class TCP
{
    //the name of the connection, not required but better for overview if you have more than 1 connections running
    public string conName = "Arduino";

    //ip/address of the server, 127.0.0.1 is for your own computer
    public string conHost = "192.168.4.1";

    //port for the server, make sure to unblock this in your router firewall if you want to allow external connections
    public int conPort = 5151;

    //a true/false variable for connection status
    public bool socketReady = false;

    TcpClient mySocket = new TcpClient();
    NetworkStream theStream;
    StreamWriter theWriter;
    StreamReader theReader;

    public bool canRead
    {
        get
        {
            if (socketReady)
            {
                return theStream.CanRead;
            }
            return false;
        }
    }
    public bool canWrite
    {
        get
        {
            if (socketReady)
            {
                return theStream.CanWrite;
            }
            return false;
        }
    }
    public bool isConnected { get { return mySocket.Connected; } }

    public TCP() { }

    public TCP(string IP, int port)
    {
        conPort = port;
        conHost = IP;
    }

    //try to initiate connection
    public string setupSocket()
    {
        try
        {
            mySocket.Connect(conHost, conPort);
        }
        catch (Exception e)
        {
            socketReady = false;
            return ("Socket error:" + e);
        }
        if (mySocket.Connected)
        {
            theStream = mySocket.GetStream();
            theWriter = new StreamWriter(theStream);
            theReader = new StreamReader(theStream);
            socketReady = true;
            return "Connected.";
        }
        socketReady = false;
        return "Socket Not Connected";
    }

    //send message to server
    public void writeSocket(string theLine)
    {
        if (!socketReady)
            return;
        String tmpString = "!" + theLine + "\r";
        theWriter.Write(tmpString);
        theWriter.Flush();
    }

    //read message from server
    public string readSocket()
    {
        if (!socketReady)
            return null;
        String result = "";
        if (theStream.DataAvailable)
        {
            Byte[] inStream = new Byte[mySocket.SendBufferSize];
            theStream.Read(inStream, 0, inStream.Length);
            result += System.Text.Encoding.UTF8.GetString(inStream);
        }
        return result;
    }

    //disconnect from the socket
    public void closeSocket()
    {
        if (!socketReady)
            return;
        theWriter.Close();
        theReader.Close();
        mySocket.Close();
        socketReady = false;
    }

    //keep connection alive, reconnect if connection lost
    public void maintainConnection()
    {
        if (!socketReady || !theStream.CanRead)
        {
            setupSocket();
        }
    }


}
