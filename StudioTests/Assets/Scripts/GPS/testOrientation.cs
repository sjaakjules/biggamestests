﻿using UnityEngine;
using System.Collections;

public class testOrientation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        var rot = GvrViewer.Instance.HeadPose.Orientation;

        transform.localRotation = rot;
    }
}
