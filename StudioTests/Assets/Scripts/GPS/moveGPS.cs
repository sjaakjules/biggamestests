﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public enum GPSfilter { smoother, average, longLatAverage, raw };

public class moveGPS : MonoBehaviour {
    public GPSfilter movementType;
    public bool rotateNorth = true;
    float lastRotation = 0;
    Quaternion initialRotation;


	// Use this for initialization
	void Start () {
        initialRotation = transform.rotation;

    }
	
	// Update is called once per frame
	void Update () {
        if (GPSManager.isConnected)
        {
            switch (movementType)
            {
                case GPSfilter.smoother:
                    transform.position = GPSManager.getSmoothPostion;
                    break;
                case GPSfilter.average:
                    transform.position = GPSManager.getAveragePostion;
                    break;
                case GPSfilter.longLatAverage:
                    transform.position = GPSManager.getAverageLatLonPostion;
                    break;
                case GPSfilter.raw:
                    transform.position = GPSManager.getGPSPosition;
                    break;
                default:
                    transform.position = GPSManager.getGPSPosition;
                    break;
            }
            if (rotateNorth)
            {
                transform.rotation = Quaternion.Euler(0, GPSManager.northRotation, 0) *initialRotation;
            }
        }
        
	}
}
