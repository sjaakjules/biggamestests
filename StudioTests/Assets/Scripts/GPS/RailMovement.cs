﻿using UnityEngine;
using System.Collections;

public class RailMovement : MonoBehaviour
{
    public Transform[] cartTransform;                     // Transform for the cart, this can be a camera location or a gameobject etc
    public Transform[] railDestinations;                // Drag and drop gameobjects as the nodes, must be more than 1

    int numNodes;                                       // Number of nodes which have been populated by the railDestinations
    int destinationNodeNumber = 1;                      // Number of index for the railNodes array which the cart is currently heading towards
    Vector3 cartPosition;                               // Current cart position
    Vector3 cartDirection;                              // A unit vector in the direction of the previous node and the destination node
    Vector3[] railNodes;                                // Array of vector3 transforms which is populated from the gamebject transforms. 

    float distanceToCart;                               // Distance of the GPS and the cart location
    Vector3 directionToCart;

    public float maxDistance = 10f;                     // Max distance before the cart stops
    public float desiredSpeed = 0.5f;                   // The desired speed when you are closest to the cart, will fall as you get further away until maxDistance is reached
    public float compleationDistance = 0.5f;            // The destance between the cart and the destination node before it moves to the next one or finishes.

    bool hasFinished = false;                           // Bool when it has finished all the nodes
    public static bool isMoving = false;                              // Bool when it is moving, this is triggered when touch is detected and isCloseToCart=true
    bool isCloseToCart = false;                         // Bool when GPS is close to cart, will trigger false when distanceToCart is greater than maxDistance
    bool hasLoadedNodes = false;                        // Bool to ensure the nodes have been loaded correctly and not get array Index errors

    bool hasAlignedNorth = false;


    // Use this for initialization
    void Start()
    {
        // Load the rail nodes
        if (railDestinations != null && railDestinations.Length >= 2)
        {
            numNodes = railDestinations.Length;
            railNodes = new Vector3[numNodes];
            for (int i = 0; i < numNodes; i++)
            {
                railNodes[i] = railDestinations[i].position;
            }
            hasLoadedNodes = true;
        }

        // Setup the rail cart
        if (hasLoadedNodes)
        {
            cartPosition = railNodes[0];
            cartDirection = railNodes[1] - railNodes[0];
            cartDirection.Normalize();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (hasLoadedNodes && GPSManager.isConnected)
        {
            if (!hasAlignedNorth && AlignNorthManager.orientationCalibrated)
            {
                foreach (Transform transform in cartTransform)
                {
                    transform.Rotate(AlignNorthManager.alignNorthEulerAngles);
                    hasAlignedNorth = true;
                }
            }
            // update distance to rail cart and trigger movement
            directionToCart = cartPosition - GPSManager.getGPSPosition;
            distanceToCart = Vector3.Distance(cartPosition, GPSManager.getGPSPosition);
            if (!isCloseToCart)
            {
                if (Vector3.Angle(directionToCart, cartDirection) <= 90)
                {
                    if (distanceToCart <= maxDistance / 2)                  // Magic number 2 is saying if you are behind the cart must be 2 times as close than max distance
                    {
                        isCloseToCart = true;
                    }
                }
                else
                {
                    if (distanceToCart <= maxDistance)
                    {
                        isCloseToCart = true;
                    }
                }
            }
            else if (distanceToCart >= maxDistance)
            {
                isCloseToCart = false;
                isMoving = false;
            }

            if (isCloseToCart && GPSManager.isMoving)
            {
                isMoving = true;
            }

            if (!hasFinished)
            {
                // update node compleation
                checkNodeFinish();

                // move cart depending on distance
                moveCart();
            }
            for (int i = 0; i < cartTransform.Length; i++)
            {
                cartTransform[i].position = cartPosition;
            }

        }
    }

    void checkNodeFinish()
    {
        if (Vector3.Distance(cartPosition, railNodes[destinationNodeNumber]) <= compleationDistance)
        {
            destinationNodeNumber++;

            if (destinationNodeNumber >= numNodes)
            {
                hasFinished = true;
                isMoving = false;
                cartPosition = railNodes[destinationNodeNumber - 1];
            }
            else
            {
                cartDirection = railNodes[destinationNodeNumber] - railNodes[destinationNodeNumber-1];
                cartDirection.Normalize();
            }
        }
    }

    void moveCart()
    {
        if (isMoving && isCloseToCart)
        {
            float scaledDistance = 1 / (distanceToCart + 1);            // This will return a number between 0 and 1 where 1 if distance is zero and 0 if distance is really large. The relationship is inverse so will decay rather than linearly drop.
            cartPosition = cartPosition + cartDirection * scaledDistance * desiredSpeed;
        }
    }
}
