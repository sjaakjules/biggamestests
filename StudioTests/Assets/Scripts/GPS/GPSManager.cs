﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GPSManager : MonoBehaviour
{
    // ///////////////////////////////////////////////////////////
    //          Static variables accessed externally
    // ///////////////////////////////////////////////////////////    
    public static float latitude, longitude;            // The latest latitude / longitude values
    public static Vector3 originPosition;               // The initial position in cardinal coordinates
    public static Vector3 iniRef;                         // Initial XYZ location
    static Vector3 position, smoothPosition, averagePosition, averageLatLonPostion;     // The vector3 positions of current GPS, (in m not 1:100)
    static float altitude;                              // Altitude according to GPS but horribly inaccurate.
    static float accuracy;                              // (m) accuracy of X/Z GPS (3m is best, 9m average)
    public static bool isSimulated;
    public static float northRotation;

    static bool gpsFix = false;                    // Bool when GPS is connected

    // Static variables for GPS settings, (must only have one value)
    static float gpsAccuracy = 0.5f;               // Accuracy of the GPS, less than 3 doesnt have much diffrence
    static float gpsUpdate = 0.5f;                 // Accuracy of the GPS, less than 3 doesnt have much diffrence

    static int maxWait = 10;                       // (seconds) Wait time for the GPS to be initialised after which progrom will exit
    static int initTime = 1;                      // (seconds) Time after GPS is connected to establish better connection.

    public static bool isMoving = false;
    int movementCount = 0;
    int positionupDates = 0;
    public int movementFilterSize = 5;
    static float lastLat, lastLon;

    static AverageFilter aveLat, aveLon, aveX, aveZ;

    public bool movingTest;


    // ///////////////////////////////////////////////////////////
    //          variables set in the inspector
    // ///////////////////////////////////////////////////////////
    public bool GeolocateOrigin = true;     // Used to toggle origin from initial GPS or specified
    public bool simGPS;                     // used when no GPS is possible, can move with arrows
    public float simSpeed = 1.5f;                  // movement speed when simulated
    public int averageSize = 10;

    // /////////////////////////////////////////////////////////
    //
    //           starting Latitude and Longitude
    //           inspector clips trailing zeros!
    //
    // /////////////////////////////////////////////////////////
    float startLon = 144.979387f;    // Start location to offset origin, also used for starting sim position
    float startLat = -37.870872f;    // Start location to offset origin, also used for starting sim position



    // The latest GPS variables 
    public LocationInfo location;                  // Location data with new information

    Vector3 gpsPosition;                    // GPS location, 1:100 units,
    //float Longitude, Latitude;              // GPS longitude and latitude

    float initLon;                          // The location where the GPS was established. 
    float initLat;                         


    float GPSupdateRate = 0.1f;             // (seconds) time rate to check for updated GPS values
    float smoothSpeed = 0.1f;               // The percentage jumps per GPSupdateRate to move to new position,

    static string status;                          // String used to report the status of the GPS

    //Vector3 smoothedPosition;               // Smoothed values, this interpolates towards latest location

    // External gameObjects and scripts used
    //Text errorText;                         // ErrorText text displayed to screen
    //Transform GPSTransform;                 // Will transform the object with the "GPSTransform" tag
    //Transform smoothGPSTransform;           // Will transform the object with the "Player" tag

    //bool foundErrorText = false;            
    //bool foundGPSTransform = false;
    //bool foundSmoothTransform = false;

    // Variables able to be seen externally
    public static Vector3 getGPSPosition { get { if (gpsFix) return position; return Vector3.zero; } }
    public static Vector3 getSmoothPostion { get { if (gpsFix) return smoothPosition; return Vector3.zero; } }
    public static Vector3 getAveragePostion { get { if (gpsFix) return averagePosition; return Vector3.zero; } }
    public static Vector3 getAverageLatLonPostion { get { if (gpsFix) return averageLatLonPostion; return Vector3.zero; } }
    public static Vector2 getLonLat { get { if (gpsFix) return new Vector2(longitude, latitude); return Vector2.zero; } }
    public static bool isConnected { get { return gpsFix; } }
    public static float Accuracy { get { if (gpsFix) return accuracy; return float.NaN; } }
    public static string Info { get { return status; } }
    
    void Awake()
    {
        aveLat = new AverageFilter(averageSize);
        aveLon = new AverageFilter(averageSize);
        aveX = new AverageFilter(averageSize);
        aveZ = new AverageFilter(averageSize);
    }

    IEnumerator Start()
    {

        isSimulated = simGPS;
        //Setting variables values on Start
        gpsFix = false;

        //STARTING LOCATION SERVICES
        // First, check if user has location service enabled
#if (UNITY_IOS && !UNITY_EDITOR)
		if (!Input.location.isEnabledByUser){
 
			//This message prints to the Editor Console
			print("Please enable location services and restart the App");
			//You can use this "status" variable to show messages in your custom user interface (GUIText, etc.)
			status = "Please enable location services\n and restart the App";
			yield return new WaitForSeconds(4);
			Application.Quit();
		}
#endif
        // Start service before querying location
        Input.location.Start(gpsAccuracy, gpsUpdate);
        Input.compass.enabled = true;
        status = "Initializing Location Services..\n";

        // Wait until service initializes
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 30 seconds
        if (maxWait < 1)
        {
            status = "Unable to initialize location services.\nPlease check your location settings\n and restart the App\n";
            yield return new WaitForSeconds(4);
            Application.Quit();
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            status = "Unable to determine your location.\nPlease check your location settings\n and restart this App\n";
            yield return new WaitForSeconds(4);
            Application.Quit();
        }

        // Access granted and location value could be retrieved
        else
        {
            status = "GPS Fix established!\n";

            if (!simGPS)
            {
                //Wait in order to find enough satellites and increase GPS accuracy
                yield return new WaitForSeconds(initTime);
                //Set position
                location = Input.location.lastData;
                if (GeolocateOrigin)
                {
                    iniRef.x = ((startLon * 20037508.34f / 180) / 100);
                    iniRef.z = (float)(System.Math.Log(System.Math.Tan((90 + startLat) * System.Math.PI / 360)) / (System.Math.PI / 180));
                    iniRef.z = ((iniRef.z * 20037508.34f / 180) / 100);
                    iniRef.y = 0;
                }
                else
                {
                    iniRef.x = ((location.longitude * 20037508.34f / 180) / 100);
                    iniRef.z = (float)(System.Math.Log(System.Math.Tan((90 + location.latitude) * System.Math.PI / 360)) / (System.Math.PI / 180));
                    iniRef.z = ((iniRef.z * 20037508.34f / 180) / 100);
                    iniRef.y = 0;
                }
                initLon = location.longitude;
                initLat = location.latitude;
            }
            else
            {
                //Simulate initialization time
                yield return new WaitForSeconds(initTime);
                //Set Position
                iniRef.x = ((initLon * 20037508.34f / 180) / 100);
                iniRef.z = (float)(System.Math.Log(System.Math.Tan((90 + initLat) * System.Math.PI / 360)) / (System.Math.PI / 180));
                iniRef.z = (iniRef.z * 20037508.34f / 180) / 100;
                iniRef.y = 0;
                initLon = startLon;
                initLat = startLat;
            }

            averageLatLonPostion = Vector3.zero;
            averagePosition = Vector3.zero;
            smoothPosition = Vector3.zero;
            //Successful GPS fix
            gpsFix = true;
        }


        //Set player's position using new location data (every "updateRate" seconds)
        //Default value for updateRate is 0.1. Increase if necessary to improve performance
        InvokeRepeating("MyPosition", 1, GPSupdateRate);


        //Get altitude and horizontal accuracy readings using new location data (Default: every 2s)
        InvokeRepeating("AccuracyAltitude", 3, 2);

        InvokeRepeating("UpdateErrorText", 2, 0.5f);

    }


    void MyPosition()
    {
        if (gpsFix)
        {
            lastLat = latitude;
            lastLon = longitude;

            if (!simGPS)
            {
                location = Input.location.lastData;
                gpsPosition.x = ((location.longitude * 20037508.34f / 180) / 100) - iniRef.x;
                gpsPosition.z = (float)(System.Math.Log(System.Math.Tan((90 + location.latitude) * System.Math.PI / 360)) / (System.Math.PI / 180));
                gpsPosition.z = ((gpsPosition.z * 20037508.34f / 180) / 100) - iniRef.z;

                // Set static long/lat variables
                longitude = location.longitude;
                latitude = location.latitude;

                northRotation = Input.compass.trueHeading;
            }
            else
            {
                //Use keyboard input to move the player
                if (Input.GetKey("up") || Input.GetKey("w"))
                {
                    gpsPosition.z += (simSpeed * GPSupdateRate)/100;
                }
                if (Input.GetKey("down") || Input.GetKey("s"))
                {
                    gpsPosition.z -= (simSpeed * GPSupdateRate)/ 100;
                }
                //Use keyboard input to move the player
                if (Input.GetKey("left") || Input.GetKey("a"))
                {
                    gpsPosition.x -= (simSpeed * GPSupdateRate)/ 100;
                }
                if (Input.GetKey("right") || Input.GetKey("d"))
                {
                    gpsPosition.x += (simSpeed * GPSupdateRate)/ 100;
                }
                if (Input.GetKey("t"))
                {
                    northRotation += 1f;
                }

                longitude = initLon + (18000 * (gpsPosition.x + iniRef.x)) / 20037508.34f;
                latitude = initLat + ((360 / Mathf.PI) * Mathf.Atan(Mathf.Exp(0.00001567855943f * (gpsPosition.z + iniRef.z)))) - 90;

            }

            if (longitude != lastLon || latitude != lastLat)
            {
                movementCount++;
            }

            if (positionupDates > movementFilterSize)
            {
                if (movementCount > 0)
                {
                    isMoving = true;
                }
                else
                {
                    isMoving = false;
                }
                positionupDates = -1;
                movementCount = 0;
            }
            positionupDates++;


            // Set static postion variables
            position = gpsPosition * 100;
            //Smoothly move pointer to updated position
            smoothPosition.x = Mathf.Lerp(smoothPosition.x, gpsPosition.x * 100, smoothSpeed);
            smoothPosition.z = Mathf.Lerp(smoothPosition.z, gpsPosition.z * 100, smoothSpeed);

            // Update the average filter
            aveLat.add(latitude);
            aveLon.add(longitude);
            aveX.add(gpsPosition.x * 100f);
            aveZ.add(gpsPosition.z * 100f);

            updateAverageValues();
            movingTest = isMoving;
        }
    }

    void AccuracyAltitude()
    {
        if (gpsFix)
        {
            altitude = location.altitude;
            accuracy = location.horizontalAccuracy;
        }
    }

    void UpdateErrorText()
    {
        status = "\n" + status;
        status = status + string.Format("GPS loc:  {0:F5} : {1:F5} : {2:F5}\n", position.x * 100, position.y * 100, position.z * 100);
        status = status + string.Format("User loc: {0:F5} : {1:F5} : {2:F5}\n", smoothPosition.x, smoothPosition.y, smoothPosition.z);
        status = status + string.Format("Altitude: {0}\n Accuracy: {1}\n", altitude, accuracy);
    }
    

    void updateAverageValues()
    {
        if (!float.IsNaN(aveX.GetAverage) && !float.IsNaN(aveZ.GetAverage))
        {
            // Get new average for positions
            averagePosition.x = aveX.GetAverage; // added in 1:1
            averagePosition.z = aveZ.GetAverage; // added in 1:1
        }

        if (!float.IsNaN(aveLat.GetAverage) && !float.IsNaN(aveLon.GetAverage))
        {
            // Get new average for positions
            averageLatLonPostion.x = ((aveLon.GetAverage * 20037508.34f / 180) / 100) - iniRef.x;
            averageLatLonPostion.z = (float)(System.Math.Log(System.Math.Tan((90 + aveLat.GetAverage) * System.Math.PI / 360)) / (System.Math.PI / 180));
            averageLatLonPostion.z = ((averageLatLonPostion.z * 20037508.34f / 180) / 100) - iniRef.z;

            averageLatLonPostion = averageLatLonPostion * 100f;         // above calculations are 1:100
        }


    }
}
