﻿using UnityEngine;
using System.Collections;
using BigGames;

public class ThunderFX : MonoBehaviour
{

    public Light thunder;
    public bool thunderOn;
    public float thunderLengthChance;

    public AudioClip[] myMusic;
    public AudioSource music;

    public bool thunderClapInSecs = true;
    public int thunderClapIn;

    public float thunderOverride;
    Color origColor;
    Color randomColor;
    float oldIntensity;
    bool chooseRandomColor;


    void Start()
    {
        music = GetComponent<AudioSource>();
        origColor = thunder.color;
    }

    void Update()
    {
        if(Magic.funtimes == true)
        {
            //thunderOn = true;

            if (chooseRandomColor == true)
            {
                randomColor = new Vector4(Random.value, Random.value, Random.value);
                chooseRandomColor = false;
            }
            thunder.color = Color.Lerp (origColor, randomColor, Time.time * 0.1f);
            oldIntensity = thunder.intensity;
            thunder.intensity = Mathf.Lerp(oldIntensity, 2.5f, Time.time * 0.01f);
        }

        else
        {
            thunder.color = Color.Lerp (randomColor, origColor, Time.time * 10);
            oldIntensity = thunder.intensity;
            thunder.intensity = Mathf.Lerp(oldIntensity, 1.5f, Time.time * 0.01f);

            chooseRandomColor = true;

            if (Magic.sonarFloat[1] < 150)
            {
                thunderOverride = (Magic.sonarFloat[1].rangeMapper(0, 100, 0.4f, 4)) / 2;
                thunder.intensity = thunderOverride;

                if (!music.isPlaying && Magic.sonarFloat[1] > 100)
                {
                    playThunderSound();
                }

            }

            else
            {
                thunder.intensity = 0.4f;

                if (thunderClapInSecs)
                {
                    thunderClapInSecs = false;
                    thunderClapIn = Random.Range(20, 60);
                    Invoke("thunderClap", thunderClapIn);

                }

                if (thunderOn)
                {
                    thunderOn = false;
                    playThunderSound();
                    doThunder();
                    thunderLengthChance = Random.Range(2, 5);
                    Invoke("resetThunder", thunderLengthChance);
                }
            }
        }
    }

    void thunderClap()
    {
        thunderOn = true;
    }

    void doThunder()
    {
        InvokeRepeating("doAThunder", 0, 0.2f);
    }

    void doAThunder()
    {
        thunder.intensity = Mathf.Lerp(3, 7, Time.deltaTime * 20);
    }

    void resetThunder()
    {
        CancelInvoke();
        thunder.intensity = 1.5f;
        thunderClapInSecs = true;
    }

    void playThunderSound()
    {
        music.clip = myMusic[Random.Range(0, myMusic.Length)];
        music.Play();
    }
}

