﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour 
{


public AudioClip[] myMusic;
public AudioSource music;

void Start()
{
    music = GetComponent<AudioSource>();
    }
    void Update()
{
    if(!music.isPlaying)
        playRandomMusic();
}


void playRandomMusic()
{
   music.clip = myMusic[Random.Range(0,myMusic.Length)];
    music.Play();
}
}
