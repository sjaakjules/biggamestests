﻿using UnityEngine;
using System.Collections;


namespace BigGames
{
    public enum SensitivityRange { low, high, none};

    public enum SensitivityStrength { Normal, Med, High}

    public static class Sensor
    {
        /// <summary>
        /// This remaps a number from given input bounds to given output bounds
        /// the reuslt is clamped for the given output bounds.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="In_L"></param>
        /// <param name="In_H"></param>
        /// <param name="Out_L"></param>
        /// <param name="Out_H"></param>
        /// <returns></returns>
        public static float rangeMapper(this float input, float In_L, float In_H, float Out_L, float Out_H, SensitivityRange sensRang, SensitivityStrength sensStr)
        {
            switch (sensRang)
            {
                case SensitivityRange.low:
                    switch (sensStr)
                    {
                        case SensitivityStrength.Normal:
                            return lowMap(input, In_L, In_H, Out_L, Out_H, 2);
                        case SensitivityStrength.Med:
                            return lowMap(input, In_L, In_H, Out_L, Out_H, 4);
                        case SensitivityStrength.High:
                            return lowMap(input, In_L, In_H, Out_L, Out_H, 6);
                        default:
                            return lowMap(input, In_L, In_H, Out_L, Out_H, 2);
                    }
                case SensitivityRange.high:
                    switch (sensStr)
                    {
                        case SensitivityStrength.Normal:
                            return highMap(input, In_L, In_H, Out_L, Out_H, 2);
                        case SensitivityStrength.Med:
                            return highMap(input, In_L, In_H, Out_L, Out_H, 4);
                        case SensitivityStrength.High:
                            return highMap(input, In_L, In_H, Out_L, Out_H, 6);
                        default:
                            return highMap(input, In_L, In_H, Out_L, Out_H, 2);
                    }
                case SensitivityRange.none:
                    return normalMap(input, In_L, In_H, Out_L, Out_H);
                default:
                    return normalMap(input, In_L, In_H, Out_L, Out_H);
            }
        }

        static float normalMap (float input, float In_L, float In_H, float Out_L, float Out_H)
        {
            float num = 0;
            float range = In_H - In_L;
            float rangeOut = Out_H - Out_L;
            input = (input < In_L ? In_L : input);
            input = (input > In_H ? In_H : input);
            num = range == 0 ? 0 : input / range;
            num = (num * rangeOut) + Out_L;
            return num;
        }

        static float lowMap(float input, float In_L, float In_H, float Out_L, float Out_H, int degree)
        {
            float num = 0;
            float range = In_H - In_L;
            float rangeOut = Out_H - Out_L;
            input = (input < In_L ? In_L : input);
            input = (input > In_H ? In_H : input);
            num = range == 0 ? 0 : input / range; // between 0 and 1
            num = Mathf.Pow(num, degree);
            num = (num * rangeOut) + Out_L;
            return num;
        }

        static float highMap(float input, float In_L, float In_H, float Out_L, float Out_H, int degree)
        {
            float num = 0;
            float range = In_H - In_L;
            float rangeOut = Out_H - Out_L;
            input = (input < In_L ? In_L : input);
            input = (input > In_H ? In_H : input);
            num = range == 0 ? 0 : input / range;
            num = Mathf.Pow(num, 1.0f/degree);
            num = (num * rangeOut) + Out_L;
            return num;
        }

        public static float rangeMapper(this float input, float In_L, float In_H, float Out_L, float Out_H)
        {
            return input.rangeMapper(In_L, In_H, Out_L, Out_H, SensitivityRange.none, SensitivityStrength.Normal);
        }
        /// <summary>
        /// This remaps the a number from a given input range to 1 and 0
        /// </summary>
        /// <param name="input"></param>
        /// <param name="In_L"></param>
        /// <param name="In_H"></param>
        /// <returns></returns>
        public static float rangeMapper(this float input, float In_L, float In_H)
        {
            return input.rangeMapper(In_L, In_H, 0, 1, SensitivityRange.none, SensitivityStrength.Normal);
        }

        /// <summary>
        /// This remaps a number from given input bounds to given output bounds
        /// the reuslt is clamped for the given output bounds.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="inputRange"></param>
        /// <param name="outputRange"></param>
        /// <returns></returns>
        public static float rangeMapper(this float input, Vector2 inputRange, Vector2 outputRange)
        {
            return input.rangeMapper(inputRange.x, inputRange.y, outputRange.x, outputRange.y, SensitivityRange.none, SensitivityStrength.Normal);
        }

        /// <summary>
        /// This remaps the a number from a given input range to 1 and 0
        /// </summary>
        /// <param name="input"></param>
        /// <param name="inputRange"></param>
        /// <param name="outputRange"></param>
        /// <returns></returns>
        public static float rangeMapper(this float input, Vector2 inputRange)
        {
            return input.rangeMapper(inputRange.x, inputRange.y, 0, 1, SensitivityRange.none, SensitivityStrength.Normal);
        }
    }
    
}