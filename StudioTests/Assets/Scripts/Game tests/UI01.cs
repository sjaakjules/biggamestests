﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI01 : MonoBehaviour {
    public Text b;
    public Text c;
    public Text a;
    public Text GPSInfo;

    // Use this for initialization
    void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
        if(Magic.funtimes == false)
        {
        b.text = "Wind is " + Magic.wind.ToString();
        c.text = "Gravity is " + Magic.grav.ToString();
        a.text = "Magic is " + Magic.magic;

        }

        if(Magic.funtimes == true)
        {

            b.text = "fun times";
            c.text = "fun times";
            a.text = "Magic is " + Magic.magic;
        }
        
        GPSInfo.text = string.Format( "Latitude:  \t{0,11:F7}\n" +
                                        "Longitude: \t{1,11:F7}\n" +
                                        "Position:  \t{2}:{3}\n" +
                                        "Accuracy:  \t{4}\n" +
                                        "GPS Moving: \t{5}\n"+ 
                                        "cart Moving: \t{6}\n"+"Arduino Sensors:\t{7}\n" , GPSManager.latitude,GPSManager.longitude,GPSManager.getGPSPosition.x,GPSManager.getGPSPosition.z,GPSManager.Accuracy,GPSManager.isMoving, RailMovement.isMoving, ArduinoWiFiManager.msgFromServer);
                                       

    }
}
