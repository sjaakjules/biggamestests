﻿using UnityEngine;
using System.Collections;
using BigGames;

public class Magic : MonoBehaviour {

    private ParticleSystem particlesObject;
    //public float sonarInt { get { return (float)ArduinoManager.sensorValues; } }
    public static float[] sonarFloat;
    public float[] defaultValue = new float[2];
    public Material[] particalMat = new Material[10];
    public Color col;
    public static float grav;
    public static float wind;

    public float windClamp = 50;
    public float windStretch = 2;
    public float gravClamp = 50;
    public float gravStretch = 1;
    public float particleChange = 50;

    public float super = 0.1f;
    public static bool funtimes = false;

    public static float magic;
    public bool seefuntimes;

    public static float reparamWind;
    public static float reparamGrav;


    // Use this for initialization
    void Start () 
	{

 
        sonarFloat = new float[ArduinoWiFiManager.sensorValues.Length];
        particlesObject = GetComponent<ParticleSystem>();
        for (int i = 0; i < sonarFloat.Length; i++)
        {
            if(sonarFloat[i] == -1)
            {
                sonarFloat[i] = defaultValue[i];
            }
        }
        
        particlesObject = GetComponent<ParticleSystem>();

        

    }
	
	// Update is called once per frame
	void Update () 
	{
        seefuntimes = funtimes;

        for (int i = 0; i < sonarFloat.Length; i++)
        {
            if (ArduinoWiFiManager.sensorValues[i] != -1)
            {
                sonarFloat[i] = (float)ArduinoWiFiManager.sensorValues[i];
            }

            else
            {
                sonarFloat[i] = defaultValue[i];
            }
        }
        //particlesObject.startLifetime = 11 - sonarFloat[1]/20;
        //col = new Vector4(sonarFloat[1]/60, (1 - sonarFloat[0]/60), sonarFloat[0]/60, 1);
        col = new Vector4(sonarFloat[1].rangeMapper(0, windClamp, 0, 1), 1 - sonarFloat[0].rangeMapper(0, windClamp, 0, 1), sonarFloat[0].rangeMapper(0, windClamp, 0, 1), 1);
        //col = new Vector4(sonarFloat[1].rangeMapper(0, windClamp, 0, 1), AudioMeasure.DbValue.rangeMapper(0, .25f, 0, 1), sonarFloat[0].rangeMapper(0, windClamp, 0, 1), 1);
        //col = new Vector4(1 - AudioMeasure.PitchValue.rangeMapper(0, 600, 0, 1), AudioMeasure.PitchValue.rangeMapper(0, 600, 0, 1), sonarFloat[0].rangeMapper(0, windClamp, 0, 1), 1);
        particlesObject.startColor = col;



        //wind = (sonarFloat[1] / windDivide) - windMinus;
        wind = sonarFloat[1].rangeMapper(0, windClamp, -windStretch, windStretch);

        var ex = particlesObject.externalForces;
        ex.multiplier = wind;


        //grav = (sonarFloat[0] / divide1 - minus) / divide2;
        grav = sonarFloat[0].rangeMapper(0, gravClamp, -gravStretch, gravStretch);
        particlesObject.gravityModifier = grav;

        /*
        reparamWind = wind.rangeMapper(-windStretch, windStretch, 1, -1);
        reparamGrav = grav.rangeMapper(-gravStretch, gravStretch, 1, -1);
        */

        magic = Mathf.Abs(wind) + Mathf.Abs(grav);

        //particlesObject.startSize = AudioMeasure.RmsValue * 100;

        //particlesObject.startSize = AudioMeasure.PitchValue.rangeMapper(0, 600, 0, 1);

        if (magic < super && magic!= 0)
        {
            particlesObject.startLifetime = 20;
            funtimes = true;
        }
        if (magic >= super)
        {
            particlesObject.startLifetime = 15;
            funtimes = false;
        }

        gameObject.GetComponent<ParticleSystemRenderer>().material = particalMat[(int)sonarFloat[1].rangeMapper(0,particleChange,0,9)];


        
        /* if ((int)sonarFloat[1]/100 < particalMat.Length)
         {
             gameObject.GetComponent<ParticleSystemRenderer>().material = particalMat[(int)sonarFloat[1] / 100];
         }
         else
         {
           gameObject.GetComponent<ParticleSystemRenderer>().material = particalMat[particalMat.Length];
         }
         */

         /*

        if (sonarFloat[1] < 10)
        {
            gameObject.GetComponent<ParticleSystemRenderer>().material = particalMat[0];
        }
        else if (sonarFloat[1] < 30)
        {
            gameObject.GetComponent<ParticleSystemRenderer>().material = particalMat[1];
        }
        else if (sonarFloat[1] < 50)
        {
            gameObject.GetComponent<ParticleSystemRenderer>().material = particalMat[2];
        }
          else if (sonarFloat[1] < 70)
        {
            gameObject.GetComponent<ParticleSystemRenderer>().material =  particalMat[3];
        }
          else if (sonarFloat[1] < 90)
        {
            gameObject.GetComponent<ParticleSystemRenderer>().material = particalMat[4];
        }
          else if (sonarFloat[1] < 90)
        {
            gameObject.GetComponent<ParticleSystemRenderer>().material = particalMat[5];
        }
          else if (sonarFloat[1] >= 90)
        {
            gameObject.GetComponent<ParticleSystemRenderer>().material = particalMat[6];
        }
        */
        



    }


}