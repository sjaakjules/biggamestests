﻿using UnityEngine;
using System.Collections;
using BigGames;

public class RandomTests : MonoBehaviour {

    public float numberIn,numberOut, HighIn, LowIn, HighOut, LowOut, numberOutL, numberOutH;

    public bool calc = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        numberOut = numberIn.rangeMapper(LowIn, HighIn, LowOut, HighOut);
        numberOutL = numberIn.rangeMapper(LowIn, HighIn, LowOut, HighOut, SensitivityRange.low, SensitivityStrength.Normal);
        numberOutH = numberIn.rangeMapper(LowIn, HighIn, LowOut, HighOut, SensitivityRange.high, SensitivityStrength.Normal);
    }
    
}
