﻿using UnityEngine;
using System.Collections;

public class Origin : MonoBehaviour {

    public GameObject target;

    // Use this for initialization
    void Start () 
	{
        transform.position = target.transform.position;
    }

}
