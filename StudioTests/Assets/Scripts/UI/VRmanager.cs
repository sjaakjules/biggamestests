﻿using UnityEngine;
using System.Collections;

public class VRmanager : MonoBehaviour {

    bool hasBegun = false;
    public bool isTriggered = false;

    public Camera VRCamera;

	// Use this for initialization

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.touchCount == 3)
        {
            int began = 0;
            int moved = 0;
            int stationary = 0;
            int ended = 0;
            foreach (Touch touch in Input.touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        began++;
                        break;
                    case TouchPhase.Moved:
                        moved++;
                        break;
                    case TouchPhase.Stationary:
                        stationary++;
                        break;
                    case TouchPhase.Ended:
                        ended++;
                        break;
                    case TouchPhase.Canceled:
                        break;
                    default:
                        break;
                }
            }
            if (!hasBegun && (began > 1 || stationary >1))
            {
                hasBegun = true;
            }
            else if (ended > 1)
            {
                isTriggered = true;
            }
        }
        if (isTriggered)
        {
            hasBegun = false;
            isTriggered = false;

            if (VRCamera.enabled)
            {
                VRCamera.enabled = false;
            }
            else
            {
                VRCamera.enabled = true;
            }
            
        }

    }
}
